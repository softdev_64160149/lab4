/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author Frame
 */
public class Table {

    private char[][] Table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'},};

    private Player player1, player2, currentPlayer;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return Table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        if (Table[row - 1][col - 1] == '-') {
            Table[row - 1][col - 1] = currentPlayer.getSymbol();
            return true;
        }
        return false;
    }

    public boolean checkWiner() {
        if (checkRow() || checkCol() || checkDiagnol()) {
            saveWin();
            return true;
        }
        return false;
    }

    private boolean checkRow() {
        for (int row = 0; row < 3; row++) {
            if (Table[row][0] == currentPlayer.getSymbol() && Table[row][1] == currentPlayer.getSymbol() && Table[row][2] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkCol() {
        for (int col = 0; col < 3; col++) {
            if (Table[0][col] == currentPlayer.getSymbol() && Table[1][col] == currentPlayer.getSymbol() && Table[2][col] == currentPlayer.getSymbol()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagnol() {
        for (int i = 0; i < 3; i++) {
            if (Table[0][0] != '-' && Table[0][0] == Table[1][1] && Table[0][0] == Table[2][2]) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (Table[0][2] != '-' && Table[0][2] == Table[1][1] && Table[0][2] == Table[2][0]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDraw() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (Table[row][col] != 'X' && Table[row][col] != 'O') {
                    return false;
                }
            }
        }
        return true;
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }
    }

    void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public void resetTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Table[i][j] = '-';
            }
        }
    }
}
