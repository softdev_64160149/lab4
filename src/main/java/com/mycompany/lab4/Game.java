/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author Frame
 */
public class Game {

    private Player player1, player2;
    private Table Table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void Play() {
        boolean isFinish = false;
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (Table.checkWiner()) {
                printTable();
                printWinner();
                printPlayers();
                if (Playagain()) {
                    Table.resetTable();
                    continue;
                }
                isFinish = true;
            }
            if (Table.checkDraw()) {
                printTable();
                printDraw();
                printPlayers();
                if (Playagain()) {
                    Table.resetTable();
                    continue;
                }
                isFinish = true;
            }
            Table.switchPlayer();
        }

    }

    private boolean Playagain() {
        String answer;
        System.out.println("Play again? (y/n)");
        Scanner sc = new Scanner(System.in);
        answer = sc.next();
        if (answer.equals("y")) {
            return true;
        } else if (answer.equals("n")) {
            return false;
        }
        return false;
    }

    private void printTable() {
        char[][] b = Table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(Table.getCurrentPlayer().getSymbol() + " : turn");
    }

    private void newGame() {
        Table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println(Table.getCurrentPlayer().getSymbol() + " ");
    }

    private void printDraw() {
        System.out.println("Draw!!!");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        Table.setRowCol(row, col);

    }

}
